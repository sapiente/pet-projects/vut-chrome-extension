let moment = require('moment');
let $ = require('jquery');

const url = "https://wis.fit.vutbr.cz/FIT/st/";

/**
 * @param {Moment} endTime - The end time of timer.
 * @param {Object} options - Timer options
 * @param {Function} [options.initTimer] - Called when timer starts.
 * @param {Function} [options.finishTimer] - Called when timer ends.
 */
module.exports.start = (endTime, options) => {
    $.get({
        url : url,
        complete: function(response) {
            let serverTime = moment(response.getResponseHeader('date'));
            
            options.callIfExists('initTimer');
            let interval = setInterval(function(){
                options.callIfExists('finishTimer');
                clearInterval(interval);
            }, endTime.toDate().getTime() - serverTime.toDate().getTime());
        }
    });
};