module.exports = {
    REGISTER_ASSESSMENT : 'registerAssessment',
    FETCH_COURSES: 'loadCourses',
    REGISTER_COURSES : 'registerCourses',
    TOGGLE_SPEED : 'slow',
    TIMEPICKER_OPTIONS: {
        minuteStep: 15,
        showSeconds: true,
        secondStep: 5,
        showMeridian: false
    }
};