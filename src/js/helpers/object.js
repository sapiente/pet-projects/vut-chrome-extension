Object.defineProperty(Object.prototype, "callIfExists", {
    value: callIfExists,
    enumerable: false
});

/**
 * Call object function if exists as property inside object.
 *
 * @param {String} name - Function name
 */
function callIfExists(name) {
    if(typeof this[name] === 'function') {
        this[name]();
    }
}


