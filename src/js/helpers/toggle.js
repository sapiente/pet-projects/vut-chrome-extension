let $ = require('jquery');

/**
 * Transform toggle element icon.
 *
 * @param {jQuery} element - Element, which has class icon.
 */
module.exports.transformToggleIcon = (element) => {
    let span = element.find('.toggle-icon');
    let angle = ((parseInt(span.attr('data-deg')) + 180) >= 360 ? 0 : 180);

    span
        .attr('data-deg', angle)
        .css('transform', `rotate(${angle}deg)`);
};


/**
 * Register toggle
 *
 * @param {jQuery} element
 * @param {Object} options
 * @param {String} options.speed
 * @param {jQuery} [options.validateForm]
 * @param {Boolean} [options.transformToggleIcon]
 */
module.exports.registerToggle = (element, options) => {
    let btn = element.find('.toggle-btn');
    let block = element.find('.toggle-block');

    btn.click(() => {
        block.toggleClass("hidden");

        if(options.validateForm)
            options.validateForm.valid();

        if(options.transformToggleIcon)
            module.exports.transformToggleIcon(btn);
    });
};