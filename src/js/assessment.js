let $ = require('jquery');
let moment = require("moment");

let serverTimer = require('./serverTimer');
let toggle = require('./helpers/toggle');
let {REGISTER_ASSESSMENT, TOGGLE_SPEED, TIMEPICKER_OPTIONS} = require('./constants');

const STORAGE_ID = 'assessment';

module.exports.BLOCK = '#assessment-block';


/**
 * @param {jQuery} block
 */
function registerAssessment(block) {
    let number = parseInt(block.find("#assessment").val().trim());
    let startTimer = block.find("#assessment-start-time").val();
    let loading = block.find('.loader');

    localStorage[`${STORAGE_ID}-startTime`] = startTimer;
    localStorage[`${STORAGE_ID}-number`] = number;

    serverTimer.start(moment(startTimer, ['hh:mm:ss']), {
        initTimer: () => {
            loading.removeClass('hidden');
        },
        finishTimer: () => {
            loading.addClass('hidden');
            chrome.tabs.getSelected(null, (tab) => {
                chrome.tabs.sendMessage(tab.id, {action: REGISTER_ASSESSMENT, options :  {number: number}}, (response) => {
                    console.log(response.success ? 'Registration success.' : 'Registration failed.');
                });
            });
        }
    });
}

/**
 * Init assessment module.
 */
module.exports.init = () => {
    let block = $("#assessment-block");
    let form = block.find("form");

    form.validate();

    form.find("button.submit").click(() => {
        if(form.valid())
            registerAssessment(block);
    });

    toggle.registerToggle(block, {
        speed: TOGGLE_SPEED,
        validateForm: form,
        transformToggleIcon: true
    });

    form.find("#assessment-start-time").timepicker(TIMEPICKER_OPTIONS);

    $("#startTime").val(localStorage[`${STORAGE_ID}-startTime`]);
};