require('jquery');
require('./helpers/object');

let constants = require('./constants');

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    switch(request.action) {
        case constants.REGISTER_COURSES:
            sendResponse({success: registerCourses(request.options)});
            break;
        case constants.REGISTER_ASSESSMENT:
            sendResponse({success: registerAssessment(request.options)});
            break;
        case constants.FETCH_COURSES:
            sendResponse({success: true, data: fetchCourses(request.options)});
            break;
        default:
            sendResponse({success: false});
    }
});

/**
 * @callback iterateSemesterCourses~action
 * @param {jQuery} row
 * @param {String} course
 */

/**
 *
 * @param {String} semester
 * @param {iterateSemesterCourses~action} action
 */
function iterateSemesterCourses(semester, action) {
    let rightSemester = true;

    $("form > table:first-of-type > tbody > tr").each(function() {
        let tr = $(this);
        let course = tr.find("th:first-child").text();

        // check for semester section
        if(tr.find("th:first-child").text().startsWith("Semestr")) {
            rightSemester = (course.replace("Semestr", '').trim() === semester);
            return;
        }

        // don't iterate bad semester courses, empty rows or header.
        if(! rightSemester || tr.find("td[colspan=10]").length != 0 || course == 'Zkr') return;

        action(tr, course);
    });
}


/**
 *
 * @param {Object} options
 * @param {String} options.semester
 *
 * @returns {Array}
 */
function fetchCourses(options) {
    let result = [];

    iterateSemesterCourses(options.semester, (row, course) => {
        result.push({
            short_name: course,
            long_name: row.find("td > b > a").text(),
            id: row.find("td > b > a").attr("href").match(/(?:\?id=)(\d+)/)[1]
        });
    });

    return result;
}

/**
 * @param {Object} options
 * @param {String[]} options.courses
 * @param {String} options.semester
 *
 * @return {Boolean} True on success, false otherwise.
 */
function registerCourses(options) {

    let form = $("form > table:first-of-type > tbody");
    options.courses.forEach((element) => {
        form.find(`input[name=course_${element}]`).attr('checked', true);
    });

    $("form input[name=submitbutton]").trigger('click');

    return true;
}


/**
 * @param {Object} options
 * @param {Number} options.number
 *
 * @return {Boolean} True on success, false otherwise.
 */
function registerAssessment(options) {
    // first is table header
    let tr = $(`form tr:nth-child(${options.number + 1})`);

    if(tr.length) {
        //id has only button input
        tr.find("input[type=submit]").trigger('click');
        return true;
    }

    return false;
}