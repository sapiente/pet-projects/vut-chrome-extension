let $ = require('jquery');
let moment = require("moment");

let toggle = require('./helpers/toggle');
let serverTimer = require('./serverTimer');
let {REGISTER_COURSES, FETCH_COURSES, TOGGLE_SPEED, TIMEPICKER_OPTIONS} = require('./constants');

const STORAGE_ID = 'courses';
module.exports.BLOCK = '#courses-block';


/**
 * @param {jQuery} block
 */
function registerCourses(block) {
    let options = {
        courses : block.find("#courses").val(),
        semester : block.find("#semester").val()
    };

    let startTimer = block.find("#courses-start-time").val();
    let loading = block.find('.loader');

    localStorage[`${STORAGE_ID}-semester`] = options.semester;

    serverTimer.start(moment(startTimer, ['hh:mm:ss']), {
        initTimer: () => {
            loading.removeClass('hidden');
        },
        finishTimer: () => {
            loading.addClass('hidden');
            chrome.tabs.getSelected(null, (tab) => {
                let sent = false;
                chrome.tabs.onUpdated.addListener((updatedTabId, changeInfo, updatedTab) => {
                    if (sent == false && changeInfo.status == 'complete' && updatedTab.id === tab.id) {
                        sent = true;
                        chrome.tabs.sendMessage(tab.id, {action: REGISTER_COURSES, options : options}, (response) => {
                            console.log(response.success ? 'Registration success.' : 'Registration failed.');
                        });
                    }
                });

                //refresh page before start
                chrome.tabs.reload(tab.id);
            });
        }
    });
}

/**
 * Load courses from page into element
 *
 * @param {jQuery} element
 */
function loadCourses(element) {
    let options = {
        semester : $("#semester").val()
    };

    chrome.tabs.getSelected(null, (tab) => {
        chrome.tabs.sendMessage(tab.id, {action: FETCH_COURSES, options : options}, (response) => {
            element
                .html('')
                .select2({
                    data: response.data.map((obj) => {return {
                        id: obj.id,
                        text: `[${obj.short_name}] ${obj.long_name}`
                    }}),
                    templateSelection: (data) => {
                        return data.text.match(/[a-zA-Z]+/)[0];
                    },
                    closeOnSelect: false
                });
        });
    });
}

/**
 * Init courses module.
 */
module.exports.init = () => {
    let block = $("#courses-block");
    let form = block.find("form");

    form.validate();

    form.find("button.submit").click(() => {
        if(form.valid())
            registerCourses(block);
    });

    toggle.registerToggle(block, {
        speed: TOGGLE_SPEED,
        validateForm: form,
        transformToggleIcon: true
    });

    //timepicker
    form.find("#courses-start-time").timepicker(TIMEPICKER_OPTIONS);

    //multiple selection
    let courses = form.find('#courses');
    form.find('#semester').change(() => loadCourses(courses));
};