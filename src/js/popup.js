require('jquery');
require('bootstrap');
require('select2');
require('../../node_modules/bootstrap-timepicker/js/bootstrap-timepicker.min');
require('jquery-validation');
require('../../node_modules/jquery-validation/dist/additional-methods.js');

require('./helpers/object');
let assessment = require('./assessment');
let courses = require('./courses');

/**
 * Simulate inline scripting.
 */
$(document).ready(function() {
    initializeFormValidation();

    courses.init();
    assessment.init();
},true);


/**
 * Initialize form validation rules.
 */
function initializeFormValidation() {
    $.validator.setDefaults({
        noValidate: true,
        errorClass: "error control-label",
        errorElement: "label",
        highlight: function (element, errorClass, validClass) {
            let formGroup = $(element).closest('.form-group');

            if($(element).attr('formnovalidate')) return;

            formGroup.removeClass('has-success has-feedback').addClass('has-error has-feedback');
            formGroup.find('span.glyphicon').remove();
            formGroup.append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
        },
        unhighlight: function (element, errorClass, validClass) {
            let formGroup = $(element).closest('.form-group');

            if($(element).attr('formnovalidate')) return;

            formGroup.removeClass('has-error has-feedback').addClass('has-success has-feedback');
            formGroup.find('span.glyphicon').remove();
            formGroup.append('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
        }
    });
}
