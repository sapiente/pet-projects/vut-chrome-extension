let webpack = require('webpack');
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = [
    {
        name: 'js',
        context: __dirname,
        entry: {
            "popup": "./src/js/popup.js",
            "content": "./src/js/content.js"
        },
        output: {
            path: __dirname + "/dist/js",
            filename: "[name].js"
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery"
            })
        ]
    },
    {
        name: 'styles',
        entry: {
            "popup": "./src/scss/popup.scss",
        },
        output: {
            path: __dirname + "/dist/css",
            filename: "[name].css"
        },
        module: {
            loaders: [
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract("style-loader", "css-loader!autoprefixer-loader!sass-loader")
                },
                {
                    test: /\.less$/,
                    loader: ExtractTextPlugin.extract("style-loader", "css-loader!autoprefixer-loader!less-loader")
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin("[name].css"),
            new CopyWebpackPlugin([
                {from: './node_modules/bootstrap/dist/css/bootstrap.min.css', to: '[name].[ext]'},
                {from: './node_modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css', to: '[name].[ext]'},
                {from: './node_modules/select2/dist/css/select2.min.css', to: '[name].[ext]'},
            ], {})
        ]
    },
    {
        name: 'fonts',
        output: {
            path: __dirname + "/dist/fonts/",
            filename: "[name].[ext]"
        },
        plugins: [
            new CopyWebpackPlugin([
                {from: {glob: './node_modules/bootstrap/dist/fonts/**', dot: true}, to: "[name].[ext]"}
            ], {})
        ]
    }
];